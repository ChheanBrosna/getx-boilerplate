import 'package:get/get.dart';
import 'package:getx_boilerplate/views/views.dart';

class Routes {
  static const String root = '/';

  static List<GetPage> pages = [
    GetPage(
      name: root,
      page: () => const StartView(),
    ),
  ];
}
