import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:getx_boilerplate/core/core.dart';

class AppConfig {
  AppConfig._();

  static final _instance = AppConfig._();
  static AppConfig get shared => _instance;

  late String _language;
  String get language => _language;

  String get baseUrl => dotenv.env[EnvKey.baseUrl.value] ?? '';
  void setLanguage(String value) {
    _language = value.toUpperCase();
  }

  Locale get languageLocale {
    switch (language.toUpperCase()) {
      case 'KH':
        return const Locale('KM', 'KH');
      case 'EN':
        return const Locale('EN', 'US');
      default:
        return const Locale('EN', 'US');
    }
  }
}
