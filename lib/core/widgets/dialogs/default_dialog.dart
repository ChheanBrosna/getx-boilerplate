import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_boilerplate/core/core.dart';

class DefaultDialogWidget extends StatelessWidget {
  const DefaultDialogWidget({
    super.key,
    required this.title,
    required this.subTitle,
    required this.onPressed,
    this.btnText = 'CLOSE',
  });

  final String title;
  final String subTitle;
  final String btnText;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: 16.paddingAllSide,
      shape: RoundedRectangleBorder(borderRadius: 16.radiusAllSize),
      child: Padding(
        padding: 16.paddingAllSide,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _closeIconWidget(),
            Text(
              title,
              style: AppTextStyle.mediumPrimarySemiBoldTextStyle,
            ),
            14.height,
            Text(
              subTitle,
              style: AppTextStyle.normalSecondaryRegularTextStyle,
            ),
            16.height,
            CustomButtonWidget(
              text: btnText,
              onPressed: onPressed,
            ),
          ],
        ),
      ),
    );
  }

  Widget _closeIconWidget() {
    return Row(
      children: [
        const Spacer(),
        InkWell(
          borderRadius: 100.radiusAllSize,
          onTap: () => Get.back(),
          child: Container(
            padding: const EdgeInsets.all(4.5),
            decoration: const BoxDecoration(
              color: AppColor.dividerColor,
              shape: BoxShape.circle,
            ),
            child: const Icon(
              Icons.close,
              color: AppColor.darkGrey,
              size: 18,
            ),
          ),
        ),
      ],
    );
  }
}
