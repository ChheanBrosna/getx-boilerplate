import 'package:flutter/material.dart';
import 'package:getx_boilerplate/core/core.dart';

class CustomButtonWidget extends StatelessWidget {
  const CustomButtonWidget({
    super.key,
    required this.text,
    required this.onPressed,
    this.bgColor = AppColor.btnPrimaryColor,
    this.borderRadius = 8,
    this.elevation = 0,
    this.width = double.infinity,
  });

  final String text;
  final VoidCallback? onPressed;
  final int borderRadius;
  final double elevation;
  final Color bgColor;
  final double? width;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: width,
      height: 50,
      color: bgColor,
      shape: RoundedRectangleBorder(borderRadius: borderRadius.radiusAllSize),
      disabledColor: Colors.blue[200],
      onPressed: onPressed,
      elevation: elevation,
      child: Text(
        text,
        style: AppTextStyle.normalWhiteSemiBoldTextStyle,
      ),
    );
  }
}
