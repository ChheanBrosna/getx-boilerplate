import 'package:flutter/material.dart';

class AppColor {
  static const primaryColor = Color(0xFF103BA0);
  static const primaryTextColor = Color(0xFF001F3F);
  static const primaryBackgroundColor = Color(0xFFF5F6F8);
  static const btnPrimaryColor = Color(0xFFA30635);

  static const secondaryColor = Color(0xFF06AEFF);
  static const secondaryTextColor = Color(0xFF66798C);

  static const dividerColor = Color(0xFFE4E5E6);
  static const lightBorderColor = Color(0xFFECEEF5);
  static const darkGrey = Color(0xFF7B808C);

  static const white = Color(0xFFFFFFFF);
  static const transparent = Color(0x00000000);
}

/// Naming Conventions:
/// Font size
/// - huge: 24
/// - large: 20
/// - medium: 18
/// - mid: 16
/// - normal: 14
/// - small: 12
/// Font weight
/// - regular: w400
/// - medium: w500
/// - semiBold: w600
/// - bold: w700
/// Format: {fontsize}{color}{weight}
class AppTextStyle {
  // Primary
  static const mediumPrimarySemiBoldTextStyle = TextStyle(
    color: AppColor.primaryTextColor,
    fontSize: 18,
    fontWeight: AppStyle._semiBold,
  );
  static const midPrimarySemiBoldTextStyle = TextStyle(
    color: AppColor.primaryTextColor,
    fontSize: 16,
    fontWeight: AppStyle._semiBold,
  );
  static const smallPrimaryRegularTextStyle = TextStyle(
    color: AppColor.primaryTextColor,
    fontSize: 14,
    fontWeight: AppStyle._regular,
  );

  // Secondary
  static const normalSecondaryRegularTextStyle = TextStyle(
    color: AppColor.secondaryTextColor,
    fontSize: 14,
    fontWeight: AppStyle._regular,
  );

  // White
  static const hugeWhiteBoldTextStyle = TextStyle(
    color: AppColor.white,
    fontSize: 24,
    fontWeight: AppStyle._bold,
  );
  static const largeWhiteMediumTextStyle = TextStyle(
    color: AppColor.white,
    fontSize: 20,
    fontWeight: AppStyle._medium,
  );
  static const mediumWhiteSemiBoldTextStyle = TextStyle(
    color: AppColor.white,
    fontSize: 18,
    fontWeight: AppStyle._semiBold,
  );
  static const midWhiteSemiBoldTextStyle = TextStyle(
    color: AppColor.white,
    fontSize: 16,
    fontWeight: AppStyle._semiBold,
  );
  static const normalWhiteSemiBoldTextStyle = TextStyle(
    color: AppColor.white,
    fontSize: 14,
    fontWeight: AppStyle._semiBold,
  );
}

class AppStyle {
  AppStyle._();

  static ThemeData themeData() {
    return ThemeData(
      primaryColor: AppColor.primaryColor,
      unselectedWidgetColor: const Color(0xFFDFE4E9),
      scaffoldBackgroundColor: AppColor.primaryBackgroundColor,
      appBarTheme: const AppBarTheme(
        backgroundColor: AppColor.white,
        elevation: 0,
        foregroundColor: AppColor.primaryTextColor,
      ),
      textTheme: _textTheme,
      canvasColor: Colors.transparent,
    );
  }

  static const TextTheme _textTheme = TextTheme(
    displayLarge: TextStyle(fontWeight: _regular, fontSize: 96.0, color: AppColor.primaryTextColor),
    displayMedium: TextStyle(fontWeight: _regular, fontSize: 60.0, color: AppColor.primaryTextColor),
    displaySmall: TextStyle(fontWeight: _regular, fontSize: 48.0, color: AppColor.primaryTextColor),
    headlineMedium: TextStyle(fontWeight: _regular, fontSize: 34.0, color: AppColor.primaryTextColor),
    headlineSmall: TextStyle(fontWeight: _regular, fontSize: 24.0, color: AppColor.primaryTextColor),
    titleLarge: TextStyle(fontWeight: _medium, fontSize: 18.0, color: AppColor.primaryTextColor),
    titleMedium: TextStyle(fontWeight: _regular, fontSize: 16.0, color: AppColor.primaryTextColor),
    titleSmall: TextStyle(fontWeight: _medium, fontSize: 14.0, color: AppColor.primaryTextColor),
    bodyLarge: TextStyle(fontWeight: _regular, fontSize: 14.0, color: AppColor.primaryTextColor),
    bodyMedium: TextStyle(fontWeight: _regular, fontSize: 12.0, color: AppColor.primaryTextColor),
    labelLarge: TextStyle(fontWeight: _semiBold, fontSize: 14.0, color: AppColor.primaryTextColor),
    bodySmall: TextStyle(fontWeight: _regular, fontSize: 16.0, color: AppColor.primaryTextColor),
    labelSmall: TextStyle(fontWeight: _regular, fontSize: 10.0, color: AppColor.primaryTextColor),
  );

  static const _regular = FontWeight.w400;
  static const _medium = FontWeight.w500;
  static const _semiBold = FontWeight.w600;
  static const _bold = FontWeight.w700;
}
