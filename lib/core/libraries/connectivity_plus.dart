import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';

class ConnectivityPlusManager {
  ConnectivityPlusManager._();

  static final ConnectivityPlusManager _instance = ConnectivityPlusManager._();
  static final ConnectivityPlusManager shared = _instance;

  final connectity = Connectivity();

  Future<bool> get isConnected async {
    final status = await connectity.checkConnectivity();
    return _isConnectedWithStatus(status);
  }

  bool _isConnectedWithStatus(ConnectivityResult status) {
    switch (status) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
      case ConnectivityResult.ethernet:
        return true;
      default:
        return false;
    }
  }
}
