import 'package:getx_boilerplate/core/core.dart';
import 'package:getx_boilerplate/main.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlLauncherManager {
  static Future<void> launch(String? url) async {
    customPrint('===== URL ===== $url');

    try {
      if (url == null) {
        throw Exception();
      }

      final uri = Uri.tryParse(url);
      if (uri == null) {
        throw Exception();
      }

      await launchUrl(uri, mode: LaunchMode.externalApplication);
    } catch (e) {
      ExceptionManager.handleException('Could not launch url: $url');
    }
  }
}
