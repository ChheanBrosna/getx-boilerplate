// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

// ignore_for_file: lines_longer_than_80_chars, constant_identifier_names
// ignore: avoid_classes_with_only_static_members
class AppTranslation {
  static Map<String, Map<String, String>> translations = {
    'EN_US': Locales.EN_US,
    'KM_KH': Locales.KM_KH,
  };
}

class LocaleKeys {
  LocaleKeys._();
  static const home = 'home';
  static const something_went_wrong = 'something_went_wrong';
  static const unexpected_error = 'unexpected_error';
  static const unable_to_establish_connection = 'unable_to_establish_connection';
  static const ok = 'ok';
}

class Locales {
  static const EN_US = {
    'home': 'Home',
    'something_went_wrong': 'Something went wrong',
    'unexpected_error': 'Unexpected error',
    'unable_to_establish_connection': 'Unable to establish connection. Please open internet and try again.',
    'ok': 'Ok',
  };
  static const KM_KH = {
    'home': 'Home',
    'something_went_wrong': 'Something went wrong',
    'unexpected_error': 'Unexpected error',
    'unable_to_establish_connection': 'Unable to establish connection. Please open internet and try again.',
    'ok': 'Ok',
  };
}
