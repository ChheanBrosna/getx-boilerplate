import 'package:get/get.dart';
import 'package:getx_boilerplate/core/core.dart';

class DialogManager {
  DialogManager._();

  static Future<dynamic> showDialog({
    required String title,
    required String subTitle,
    Function()? onPressed,
    String? btnText,
  }) async {
    return Get.dialog(
      barrierDismissible: false,
      DefaultDialogWidget(
        title: title,
        subTitle: subTitle,
        onPressed: () => Get.back(),
      ),
    );
  }

  static Future<dynamic> showConnectionDialog() async {
    return Get.dialog(
      barrierDismissible: false,
      DefaultDialogWidget(
        title: LocaleKeys.something_went_wrong.tr,
        subTitle: LocaleKeys.unable_to_establish_connection.tr,
        btnText: LocaleKeys.ok.tr.toUpperCase(),
        onPressed: () => Get.back(),
      ),
    );
  }
}
