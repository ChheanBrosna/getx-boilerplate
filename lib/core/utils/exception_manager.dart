import 'package:dio/dio.dart';
import 'package:getx_boilerplate/core/utils/utils.dart';

class ExceptionManager {
  ExceptionManager._();

  static void handleException(dynamic error, {bool alert = true}) {
    if (!alert) {
      return;
    }

    const String title = 'Error';
    String subTitle = 'UnKnown Error';

    if (error is DioException) {
      final res = error.response;
      if (res != null) {
        if (res.data is Map) {
          subTitle = res.data['message'];
        } else {
          subTitle = res.data;
        }
      }
    } else if (error is String) {
      subTitle = error;
    } else {
      subTitle = 'Something went wrong';
    }

    DialogManager.showDialog(
      title: title,
      subTitle: subTitle,
    );
  }
}
