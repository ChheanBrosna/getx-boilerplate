class UserRepository {
  UserRepository._();

  static final UserRepository _instance = UserRepository._();
  static UserRepository get shared => _instance;
}
