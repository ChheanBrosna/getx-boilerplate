class EndPoints {
  static const _authBase = '/authentication';

  static String get getProfile => '$_authBase/get-profile';
  static String get todo => 'todos/1';
}
