import 'package:flutter/material.dart';

extension IntX on int {
  Duration get minute => Duration(minutes: this);
  Duration get second => Duration(seconds: this);
  Duration get milliSecond => Duration(milliseconds: this);
  Duration get microseconds => Duration(microseconds: this);

  Widget get width => SizedBox(width: toDouble());
  Widget get height => SizedBox(height: toDouble());

  BorderRadius get radiusAllSize => BorderRadius.circular(toDouble());

  EdgeInsets get paddingAllSide => EdgeInsets.all(toDouble());
  EdgeInsets get paddingHorizontalSide => EdgeInsets.symmetric(horizontal: toDouble());
  EdgeInsets get paddingVerticalSide => EdgeInsets.symmetric(vertical: toDouble());
  EdgeInsets get paddingLeftSide => EdgeInsets.only(left: toDouble());
  EdgeInsets get paddingRightSide => EdgeInsets.only(right: toDouble());
  EdgeInsets get paddingBottomSide => EdgeInsets.only(bottom: toDouble());
  EdgeInsets get paddingTopSide => EdgeInsets.only(top: toDouble());
}
