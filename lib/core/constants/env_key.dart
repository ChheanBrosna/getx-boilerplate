enum EnvKey {
  baseUrl('BASE_URL'),
  ;

  final String value;
  const EnvKey(this.value);
}
