class BasePath {
  static String get assetBasePath => 'assets/images/';
}

enum AssetPath {
  facePlaceHolder('face_placeholder'),
  ;

  final String key;
  const AssetPath(this.key);

  String get localPath {
    switch (this) {
      case AssetPath.facePlaceHolder:
        return '${BasePath.assetBasePath}$key.jpg';
    }
  }
}
