import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:getx_boilerplate/core/core.dart';
import 'package:getx_boilerplate/flavor/flavor.dart';
import 'package:getx_boilerplate/routes.dart';
import 'package:logging/logging.dart';

Future<void> main() async {
  runZonedGuarded<Future<void>>(() async {
    WidgetsFlutterBinding.ensureInitialized();

    await _setAppSystemPreferences();

    await _initEnvironment();

    await _initServices();

    await dotenv.load(fileName: '.env');

    if (kDebugMode) {
      Logger.root.level = Level.ALL;
      Logger.root.onRecord.listen(customPrint);
    } else {
      Logger.root.level = Level.OFF;
    }

    runApp(const MyApp());
  }, (exception, trace) {
    ExceptionManager.handleException(exception);
  });
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'GetX Boilerplate',
      translationsKeys: AppTranslation.translations,
      locale: AppConfig.shared.languageLocale,
      fallbackLocale: const Locale('EN', 'US'),
      debugShowCheckedModeBanner: false,
      theme: AppStyle.themeData(),
      initialRoute: Routes.root,
      getPages: Routes.pages,
      builder: (context, child) {
        final MediaQueryData mediaQueryData = MediaQuery.of(context);

        final TextScaler constrainedTextScaleFactor =
            mediaQueryData.textScaler.clamp(minScaleFactor: 1.1, maxScaleFactor: 1.125);

        return MediaQuery(
          data: mediaQueryData.copyWith(textScaler: constrainedTextScaleFactor),
          child: child!,
        );
      },
    );
  }
}

void customPrint(dynamic value) {
  if (kDebugMode) {
    debugPrint('[INFO] : $value');
  }
}

Future<void> _setAppSystemPreferences() async {
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
}

Future<void> _initEnvironment() async {
  AppConfig.shared.setLanguage('EN');
}

Future<void> _initServices() async {}
