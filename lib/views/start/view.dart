import 'package:flutter/material.dart';
import 'package:getx_boilerplate/core/core.dart';

class StartView extends StatelessWidget {
  const StartView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Splash View')),
      body: Padding(
        padding: 16.paddingAllSide,
        child: Center(
          child: CustomButtonWidget(
            text: 'CONTINUE',
            onPressed: () {
              DialogManager.showDialog(
                title: 'Something went wrong',
                subTitle: 'The customer has been removed. Please register again.',
              );
            },
          ),
        ),
      ),
    );
  }
}
